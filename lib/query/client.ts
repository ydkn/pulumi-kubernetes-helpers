import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as k8sClient from "@kubernetes/client-node";

export class Client {
  private client: pulumi.Output<k8sClient.CoreV1Api>;

  /**
   * Kubernetes API client.
   * @param kubeconfig The kubeconfig for the connection
   */
  constructor(kubeconfig: pulumi.Input<string>) {
    this.client = pulumi.output([kubeconfig]).apply(([kubeconfig]) => {
      const kc = new k8sClient.KubeConfig();
      kc.loadFromString(kubeconfig);
      return kc.makeApiClient(k8sClient.CoreV1Api);
    });
  }

  public nodes(): pulumi.Output<pulumi.Output<k8s.core.v1.Node>[]> {
    return this.client.apply((client) => {
      const promise = new Promise<k8sClient.V1Node[]>((resolve, reject) => {
        client
          .listNode()
          .then((res) => {
            resolve(res.body.items);
          })
          .catch((error) => {
            reject(error);
          });
      });

      return pulumi.all([promise]).apply(([nodes]) => {
        const outputNodes: pulumi.Output<k8s.core.v1.Node>[] = [];

        for (const n of nodes) {
          n.apiVersion = "v1";
          n.kind = "Node";

          const node = pulumi.output(n as any as k8s.core.v1.Node);

          outputNodes.push(node);
        }

        return outputNodes;
      });
    });
  }
}
